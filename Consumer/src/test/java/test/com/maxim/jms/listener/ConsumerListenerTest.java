package test.com.maxim.jms.listener;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.maxim.jms.listener.ConsumerListener;

public class ConsumerListenerTest {

	private TextMessage message;

	//private ApplicationContext context;
	
	private ApplicationContext applicationContext;
	private ConsumerListener listener;
	private String json = "{vendorName:\"Microsofttest33\",firstName:\"BobTest33\",lastName:\"SmithTest33\",address:\"123 Main test\",city:\"TulsaTest333\",state:\"OKTest333\",zip:\"71345Test333\",email:\"Bob@microsoft.test333\",phoneNumber:\"test-123-test333\"}";

	@Before
	public void setUp() throws Exception {
		System.out.println("qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq");
		//context = new ClassPathXmlApplicationContext("/spring/application-config.xml");
		applicationContext = new ClassPathXmlApplicationContext("/spring/application-config.xml");
		System.out.println(applicationContext);
		listener = (ConsumerListener)applicationContext.getBean("consumerListener");
		System.out.println(listener);
		message = createMock(TextMessage.class);
		System.out.println(message);
	}

	@After
	public void tearDown() throws Exception {
		((ConfigurableApplicationContext)applicationContext).close();
	}

	@Test
	public void testOnMessage() throws JMSException {

		expect(message.getText()).andReturn(json);
		replay(message);

		// ConsumerListener consumerListener = new ConsumerListener();
		listener.onMessage(message);
		//assertNull(message);
		verify(message);
	}

}
