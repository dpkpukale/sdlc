package com.maxim.jms.adapter;

import java.net.UnknownHostException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

@Component
public class ConsumerAdapter {

	private static Logger logger = LogManager.getLogger(ConsumerAdapter.class.getName());

	public void sendToMongo(String json) throws UnknownHostException {
		logger.info("inside sendToMongo() of ConsumerAdapter class");

		MongoClient mongoClient = new MongoClient();
		DB db = mongoClient.getDB("vendor");
		DBCollection dbCollection = db.getCollection("contact");
		logger.info("converting JSON to DB object");
		DBObject dbObject = (DBObject) JSON.parse(json);
		dbCollection.insert(dbObject);
		logger.info("sent to MongoDB from consumeradapter");

	}

}
