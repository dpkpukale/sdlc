package com.maxim.jms.listener;

import java.net.UnknownHostException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.maxim.jms.adapter.ConsumerAdapter;

@Component
public class ConsumerListener implements MessageListener {

	@Autowired
	JmsTemplate jmsTemplate;

	@Autowired
	ConsumerAdapter consumerAdapter;

	private static Logger logger = LogManager.getLogger(ConsumerListener.class.getName());

	public void onMessage(Message message) {

		// System.out.println("Message from consumer - in OnMessage");
		logger.info("Message from consumer - in OnMessage");

		String json = null;

		if (message instanceof TextMessage) {
			try {
				json = ((TextMessage) message).getText();
				logger.info("sending JSON to DB - " + json);
				consumerAdapter.sendToMongo(json);
			} catch (JMSException e) {
				logger.error("Message: " + json);
				jmsTemplate.convertAndSend(json);
			} catch (UnknownHostException e) {
				logger.error("Message: " + json);
				jmsTemplate.convertAndSend(json);
			} catch (Exception e) {
				logger.error("Message: " + json);
				jmsTemplate.convertAndSend(json);
			}
		}
	}

}
