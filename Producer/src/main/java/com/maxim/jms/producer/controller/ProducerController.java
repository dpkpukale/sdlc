package com.maxim.jms.producer.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.maxim.jms.producer.model.Vendor;
import com.maxim.jms.producer.service.MessageService;

@Controller
public class ProducerController {

	private static Logger logger = LogManager.getLogger(ProducerController.class.getName());

	@Autowired
	private MessageService messageSerevice;

	@RequestMapping("/")
	public String renderVendorPage(Vendor vendor, Model model) {
		logger.info("rendering index.jsp");
		return "index";
	}

	@RequestMapping(value = "/vendor", method = RequestMethod.POST)
	public ModelAndView process(@ModelAttribute("vendor") Vendor vendor, Model model) {

		logger.info("processing vendor object");
		logger.info(vendor.toString());

		messageSerevice.process(vendor);
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("index");
		modelAndView.addObject("message", "vendor added successfully");
		
		Vendor vendor1 = new Vendor();
		modelAndView.addObject("vendor", vendor1);
		
		return modelAndView;

	}
}
