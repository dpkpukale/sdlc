package com.maxim.jms.producer.model;

import org.springframework.stereotype.Component;

@Component
public class Vendor {

	private String vendorName;
	private String firstName;
	private String lastName;
	private String address;
	private String zipcode;
	private String city;
	private String email;
	private String phoneNumber;

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "Vendor [vendorName=" + getVendorName() + ", firstName=" + getFirstName() + ", lastName=" + getLastName()
				+ ", address=" + getAddress() + ", zipcode=" + getZipcode() + ", city=" + getCity() + ", email="
				+ getEmail() + ", phoneNumber=" + getPhoneNumber() + ", getVendorName()=" + getVendorName()
				+ ", getFirstName()=" + getFirstName() + ", getLastName()=" + getLastName() + ", getAddress()="
				+ getAddress() + ", getZipcode()=" + getZipcode() + ", getCity()=" + getCity() + ", getEmail()="
				+ getEmail() + ", getPhoneNumber()=" + getPhoneNumber() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}

}
